package it.unibo.coordination.prologx;

import alice.tuprolog.Term;

public interface PrologSerializable {
    Term toTerm();
}
