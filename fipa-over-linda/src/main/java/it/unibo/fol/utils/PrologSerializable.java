package it.unibo.fol.utils;

import alice.tuprolog.Term;

public interface PrologSerializable {
    Term toTerm();
}
