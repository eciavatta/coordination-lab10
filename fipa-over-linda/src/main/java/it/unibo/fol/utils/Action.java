package it.unibo.fol.utils;

@FunctionalInterface
public interface Action<E extends Exception> {
    void execute() throws E;
}