package it.unibo.fol.agents.behaviours;

import it.unibo.fol.agents.AgentController;
import it.unibo.fol.agents.behaviours.messages.Message;
import it.unibo.fol.ts.logic.LogicTemplate;
import it.unibo.fol.ts.logic.LogicTuple;
import it.unibo.fol.ts.logic.LogicTupleSpace;

import java.util.concurrent.CompletableFuture;

abstract class Receive extends LindaOperation<LogicTuple> {

    @Override
    public final CompletableFuture<LogicTuple> invokeOnTupleSpace(AgentController ctl, LogicTupleSpace tupleSpace) {
        return tupleSpace.take(new LogicTemplate(Message.getTemplate(ctl.getAgentId())));
    }

    @Override
    public final String getTupleSpaceName(AgentController ctl) {
        return "inbox-" + ctl.getAgentId();
    }

    @Override
    public final void onResult(AgentController ctl, LogicTuple result) throws Exception {
        final Message parsedMessage = Message.fromTerm(result.getTuple());
        ctl.log("Receiving `%s` from agent \"%s\"", parsedMessage.getPayload(), parsedMessage.getSender());
        onMessageReceived(ctl, parsedMessage);
    }

    public abstract void onMessageReceived(AgentController ctl, Message message) throws Exception;
}
