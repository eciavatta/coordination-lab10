package it.unibo.fol.agents.behaviours.messages;

import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import it.unibo.fol.utils.PrologSerializable;

public enum Performative implements PrologSerializable {
    ACCEPT_PROPOSAL,
    AGREE,
    CANCEL,
    CFP,
    CONFIRM,
    DISCONFIRM,
    FAILURE,
    INFORM,
    INFORM_IF,
    INFORM_REF,
    NOT_UNDERSTOOD,
    PROPAGATE,
    PROPOSE,
    PROXY,
    QUERY_IF,
    QUERY_REF,
    REFUSE,
    REJECT_PROPOSAL,
    REQUEST,
    REQUEST_WHEN,
    REQUEST_WHENEVER,
    SUBSCRIBE;

    private static Term TEMPLATE = Term.createTerm("performative(_)");

    public Struct toAtom() {
        return new Struct(this.name().toLowerCase());
    }

    public static Performative fromTerm(Term term) {
        if (term instanceof Struct) {
            final Struct struct = ((Struct) term);
            if (struct.getArity() == 1 && struct.getName().equals("performative")) {
                return fromAtom(struct.getArg(0));
            }
        }
        throw new IllegalArgumentException(term.toString());

    }

    public static Performative fromAtom(Term term) {
        if (term instanceof Struct) {
            return fromString(((Struct) term).getName());
        } else {
            throw new IllegalArgumentException(term.toString());
        }
    }

    public static Performative fromString(String string) {
        return Performative.valueOf(string.toUpperCase());
    }

    public static Term getTemaplte() {
        return TEMPLATE;
    }

    @Override
    public Term toTerm() {
        return new Struct("performative", toAtom());
    }
}
