package it.unibo.fol.agents.behaviours;

import it.unibo.fol.agents.Agent;
import it.unibo.fol.agents.AgentController;
import it.unibo.fol.utils.Action;
import it.unibo.fol.utils.Action1;

import java.util.function.Supplier;

@FunctionalInterface
public interface Behaviour {
    void execute(AgentController ctl) throws Exception;

    default boolean isPaused() {
        return false;
    }

    default boolean isOver() {
        return true;
    }

    default Behaviour deepClone() {
        return this;
    }

    default Behaviour addTo(Agent agent) {
        agent.addBehaviour(this);
        return this;
    }

    default Behaviour removeFrom(Agent agent) {
        agent.removeBehaviour(this);
        return this;
    }

    default Behaviour andThen(Behaviour b, Behaviour... bs) {
        return new Sequence(this, b, bs);
    }

    default Behaviour andThen(Action<? extends Exception> action) {
        return andThen(Behaviours.of(action));
    }

    default Behaviour andThen(Action1<AgentController, ? extends Exception> action) {
        return andThen(Behaviours.of(action));
    }

    default Behaviour repeatWhile(Supplier<Boolean> condition) {
        return new DoWhile(Behaviour.this) {

            @Override
            public boolean condition() {
                return condition.get();
            }

        };
    }

    default Behaviour repeatUntil(Supplier<Boolean> condition) {
        return repeatWhile(() -> !condition.get());
    }

    default Behaviour repeatForEver() {
        return repeatWhile(() -> true);
    }

}