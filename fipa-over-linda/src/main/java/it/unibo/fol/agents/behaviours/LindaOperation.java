package it.unibo.fol.agents.behaviours;

import it.unibo.fol.agents.AgentController;
import it.unibo.fol.ts.logic.LogicTupleSpace;

import java.util.concurrent.CompletableFuture;

abstract class LindaOperation<T> extends Await<T> {

    @Override
    public final CompletableFuture<T> getPromise(AgentController ctl) {
        return invokeOnTupleSpace(ctl, ctl.getTupleSpace(getTupleSpaceName(ctl)));
    }

    public abstract CompletableFuture<T> invokeOnTupleSpace(AgentController ctl, LogicTupleSpace tupleSpace);

    public abstract String getTupleSpaceName(AgentController ctl);

}
