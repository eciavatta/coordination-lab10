package it.unibo.fol.agents.behaviours;

import it.unibo.fol.agents.AgentController;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;

class Wait implements Behaviour {

    private final Duration duration;
    private boolean started;
    private OffsetDateTime clock;
    private boolean ended;

    public Wait(Duration duration) {
        this.duration = duration;
    }

    @Override
    public Behaviour deepClone() {
        return new Wait(duration);
    }

    @Override
    public void execute(AgentController ctl) throws Exception {
        if (started) {
            if (ChronoUnit.MILLIS.between(clock, OffsetDateTime.now()) >= duration.toMillis()) {
                ended = true;
            }
        } else {
            started = true;
            clock = OffsetDateTime.now();
        }
    }

    @Override
    public boolean isOver() {
        return ended;
    }
}
