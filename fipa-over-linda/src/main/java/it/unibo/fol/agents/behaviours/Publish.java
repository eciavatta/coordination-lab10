package it.unibo.fol.agents.behaviours;

import alice.tuprolog.Term;
import it.unibo.fol.agents.AgentController;
import it.unibo.fol.agents.behaviours.messages.Notification;
import it.unibo.fol.agents.behaviours.messages.Subscription;
import it.unibo.fol.ts.logic.LogicTemplate;
import it.unibo.fol.ts.logic.LogicTuple;

import java.util.List;
import java.util.stream.Collectors;

abstract class Publish implements Behaviour {

    private List<Subscription> subscriptions;

    private final Behaviour getSubscriptionBehaviour = Behaviours.linda(
            ctl -> ctl.getEnvironment().getBlackboardTupleSpaceName(),
            (ctl, ts) -> ts.readAll(new LogicTemplate(getSubscriptionTemplate(ctl))),
            (ctl, t) -> {
                final Notification notification = getNotification(ctl);
                subscriptions = t.stream()
                        .map(LogicTuple::getTuple)
                        .map(Subscription::fromTerm)
                        .filter(notification::matches)
                        .collect(Collectors.toList());
                ctl.log("Found the following subscriptions interested in topic %s: %s", notification.getTopic(), subscriptions);
            }
        );

    private final Behaviour pushNotificationsBehaviour = Behaviours.linda(
            ctl -> ctl.getEnvironment().getBlackboardTupleSpaceName(),
            (ctl, ts) -> {
                final Term notificationTerm = getNotification(ctl).toTerm();
                final List<LogicTuple> notificationsTuples = subscriptions.stream()
                        .map(it -> notificationTerm)
                        .map(LogicTuple::new)
                        .collect(Collectors.toList());
                return ts.writeAll(notificationsTuples);
            },
            (ctl, t) -> {
                ctl.log("Published %s for %d subscribers", getNotification(ctl), subscriptions.size());
            }
        );

    @Override
    public void execute(AgentController ctl) throws Exception {
        if (subscriptions == null) {
            getSubscriptionBehaviour.execute(ctl);
        } else {
            pushNotificationsBehaviour.execute(ctl);
        }
    }

    @Override
    public boolean isPaused() {
        if (subscriptions == null) {
            return getSubscriptionBehaviour.isPaused();
        } else {
            return pushNotificationsBehaviour.isPaused();
        }
    }

    @Override
    public boolean isOver() {
        if (subscriptions == null) {
            return getSubscriptionBehaviour.isOver();
        } else {
            return pushNotificationsBehaviour.isOver();
        }
    }

    @Override
    public Behaviour deepClone() {
        throw new IllegalStateException("You must override the deepClone method in class " + this.getClass().getName());
    }

    private Object getTopic(AgentController ctl) {
        return getNotification(ctl).getTopic();
    }

    private Term getSubscriptionTemplate(AgentController ctl) {
        return Subscription.getTemplate(getTopic(ctl));
    }

    public abstract Notification getNotification(AgentController ctl);
}
