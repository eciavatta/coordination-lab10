package it.unibo.fol.agents.behaviours;

import alice.tuprolog.Term;
import it.unibo.fol.agents.AgentController;
import it.unibo.fol.agents.behaviours.messages.Notification;
import it.unibo.fol.agents.behaviours.messages.Subscription;
import it.unibo.fol.ts.logic.LogicTemplate;
import it.unibo.fol.ts.logic.LogicTuple;

abstract class Subscribe implements Behaviour {

    private boolean subscribed = false;

    private final Behaviour subscribeBehaviour = Behaviours.sequence(
            Behaviours.linda(
                    ctl -> ctl.getEnvironment().getBlackboardTupleSpaceName(),
                    (ctl, ts) -> ts.write(new LogicTuple(getSubscription(ctl).toTerm())),
                    (ctl, t) -> {
                        final Subscription subscription = Subscription.fromTerm(t.getTuple());
                        onSubscribed(ctl, subscription);
                        ctl.log("Successfully subscribed for topic: %s", subscription.getTopic());
                        subscribed = true;
                    }
            )
        );

    private final Behaviour handleNotificationsBehaviour = Behaviours.linda(
            ctl -> ctl.getEnvironment().getBlackboardTupleSpaceName(),
            (ctl, ts) -> ts.take(new LogicTemplate(getNotificationTemplate(ctl))),
            (ctl, t) -> {
                final Notification notification = Notification.fromTerm(t.getTuple());
                ctl.log("Received notification for topic %s, content: %s", notification.getTopic(), notification.getContent());
                onNotified(ctl, notification);
            }).repeatForEver();

    @Override
    public void execute(AgentController ctl) throws Exception {
        if (subscribed) {
            handleNotificationsBehaviour.execute(ctl);
        } else {
            subscribeBehaviour.execute(ctl);
        }
    }

    @Override
    public boolean isPaused() {
        if (subscribed) {
            return handleNotificationsBehaviour.isPaused();
        } else {
            return subscribeBehaviour.isPaused();
        }
    }

    @Override
    public boolean isOver() {
        if (subscribed) {
            return handleNotificationsBehaviour.isOver();
        } else {
            return subscribeBehaviour.isOver();
        }
    }

    @Override
    public Behaviour deepClone() {
        throw new IllegalStateException("You must override the deepClone method in class " + this.getClass().getName());
    }

    private Term getNotificationTemplate(AgentController ctl) {
        return Notification.getTemplate(getTopic(ctl));
    }

    public abstract Object getTopic(AgentController ctl);

    private Subscription getSubscription(AgentController ctl) {
        return new Subscription(ctl.getAgentId(), getTopic(ctl));
    }

    public abstract void onSubscribed(AgentController ctl, Subscription subscription) throws Exception;

    public abstract void onNotified(AgentController ctl, Notification subscription) throws Exception;
}
