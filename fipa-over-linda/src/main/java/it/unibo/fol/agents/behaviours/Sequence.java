package it.unibo.fol.agents.behaviours;

import it.unibo.fol.agents.AgentController;

import java.util.*;
import java.util.stream.Collectors;

class Sequence implements Behaviour {

    private final Deque<Behaviour> subBehaviours = new LinkedList<>();

    public Sequence(Collection<Behaviour> bs) {
        if (bs.isEmpty()) throw new IllegalArgumentException();
        subBehaviours.addAll(bs);
    }

    public Sequence(Behaviour b, Behaviour... bs) {
        subBehaviours.add(b);
        subBehaviours.addAll(Arrays.asList(bs));
    }

    public Sequence(Behaviour b1, Behaviour b2, Behaviour... bs) {
        subBehaviours.add(b1);
        subBehaviours.add(b2);
        subBehaviours.addAll(Arrays.asList(bs));
    }

    @Override
    public Behaviour deepClone() {
        return new Sequence(subBehaviours.stream().map(Behaviour::deepClone).collect(Collectors.toList()));
    }

    @Override
    public boolean isPaused() {
        Behaviour nextBehaviour = subBehaviours.peek();
        return nextBehaviour != null && nextBehaviour.isPaused();
    }

    @Override
    public void execute(AgentController ctl) throws Exception {
        final Behaviour b = subBehaviours.poll();
        try {
            if (b != null) {
                b.execute(ctl);
            }
        } finally {
            if (b != null && !b.isOver()) {
                subBehaviours.addFirst(b);
            }
        }

    }

    protected List<Behaviour> getSubBehaviours() {
        return new ArrayList<>(subBehaviours);
    }

    protected int countResidualSubBehaviours() {
        return subBehaviours.size();
    }

    protected void addSubBehaviours(Collection<Behaviour> subBehaviours) {
        this.subBehaviours.addAll(subBehaviours);
    }

    @Override
    public boolean isOver() {
        return subBehaviours.isEmpty();
    }

}
