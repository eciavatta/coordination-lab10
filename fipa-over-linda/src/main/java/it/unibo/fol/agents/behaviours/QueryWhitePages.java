package it.unibo.fol.agents.behaviours;

import it.unibo.fol.agents.AgentController;
import it.unibo.fol.agents.AgentId;
import it.unibo.fol.ts.logic.LogicTemplate;
import it.unibo.fol.ts.logic.LogicTuple;
import it.unibo.fol.ts.logic.LogicTupleSpace;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

abstract class QueryWhitePages extends LindaOperation<Collection<? extends LogicTuple>> {


    @Override
    public final CompletableFuture<Collection<? extends LogicTuple>> invokeOnTupleSpace(AgentController ctl, LogicTupleSpace tupleSpace) {
        return tupleSpace.readAll(new LogicTemplate(AgentId.getTemplate()));
    }

    @Override
    public final String getTupleSpaceName(AgentController ctl) {
        return ctl.getEnvironment().getWhitePagesTupleSpaceName();
    }

    @Override
    public final void onResult(AgentController ctl, Collection<? extends LogicTuple> result) throws Exception {
        onQueryResult(
                ctl,
                result.stream().map(LogicTuple::getTuple).map(AgentId::fromTerm).collect(Collectors.toList())
        );
    }

    public abstract void onQueryResult(AgentController ctl, List<AgentId> agentIds) throws Exception;


}
