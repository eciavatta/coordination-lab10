package it.unibo.fol.agents.behaviours;

import it.unibo.fol.agents.AgentController;

import java.util.concurrent.CompletableFuture;

abstract class Await<T> implements Behaviour {

    private enum Phase { CREATED, PAUSED, COMPLETED, DONE }

    private Phase phase = Phase.CREATED;
    private CompletableFuture<T> promiseCache;

    @Override
    public void execute(AgentController ctl) throws Exception {
        if (phase == Phase.CREATED) {
            promiseCache = getPromise(ctl);
            promiseCache.thenRunAsync(() -> {
                phase = Phase.COMPLETED;
                ctl.resumeIfPaused();
            }, ctl.getEngine());
            phase = Phase.PAUSED;
        } else if (phase == Phase.COMPLETED) {
            onResult(ctl, promiseCache.get());
            phase = Phase.DONE;
        } else {
            throw new IllegalStateException("This should never happen");
        }
    }

    public abstract void onResult(AgentController ctl, T result) throws Exception;

    public abstract CompletableFuture<T> getPromise(AgentController ctl);


    @Override
    public boolean isPaused() {
        return phase == Phase.PAUSED;
    }

    @Override
    public boolean isOver() {
        return phase == Phase.DONE;
    }

    @Override
    public Behaviour deepClone() {
        throw new IllegalStateException("You must override the deepClone method in class " + this.getClass().getName());
    }
}
