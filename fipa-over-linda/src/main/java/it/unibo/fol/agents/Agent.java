package it.unibo.fol.agents;

import it.unibo.fol.agents.behaviours.Behaviour;
import it.unibo.fol.agents.behaviours.Behaviours;
import it.unibo.fol.ts.logic.LogicTemplate;
import it.unibo.fol.ts.logic.LogicTuple;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;

public class Agent extends BaseAgent {

    private final Queue<Behaviour> toDoList = new LinkedList<>();

    public Agent(String name) {
        super(name);
    }

    public Agent(AgentId id) {
        super(id);
    }

    @Override
    public void onBegin() throws Exception {
        Behaviours.linda(getEnvironment().getWhitePagesTupleSpaceName(), ts -> ts.write(new LogicTuple(getAgentId().toTerm())), (ctl, t) -> {
                log("Registered my name on the white pages");
            }).andThen(this::setup)
            .addTo(this);
    }

    public void setup() {
        // Does nothing by default
    }

    @Override
    public void onRun() throws Exception {
        if (toDoList.isEmpty()) {
            pause();
        } else {
            final Queue<Behaviour> skipped = new LinkedList<>();
            Behaviour behaviour = toDoList.poll();
            try {
                while (behaviour != null && behaviour.isPaused()) {
                    skipped.add(behaviour);
                    behaviour = toDoList.poll();
                }

                if (behaviour != null) {
                    behaviour.execute(AgentController.of(this));
                } else {
                    pause();
                }
            } finally {
                if (behaviour != null && !behaviour.isOver()) {
                    toDoList.add(behaviour);
                }
                toDoList.addAll(skipped);
            }
        }
    }

    @Override
    public final void onEnd() throws Exception {
        getEnvironment().getWhitePagesTupleSpace().tryTake(new LogicTemplate(getAgentId().toTerm()))
                .thenRunAsync(() -> log("Unregistered my name from the white pages"), getEnvironment().getEngine())
                .thenRunAsync(this::tearDown);
    }

    public void tearDown() {
        // Does nothing by default
    }

    public void addBehaviour(Collection<Behaviour> behaviours) {
        if (behaviours.size() > 0) {
            toDoList.addAll(behaviours);
            resumeIfPaused();
        }
    }

    public void addBehaviour(Behaviour... behaviours) {
        addBehaviour(Arrays.asList(behaviours));
    }

    public void removeBehaviour(Collection<Behaviour> behaviours) {
        if (behaviours.size() > 0) {
            toDoList.removeAll(behaviours);
            resumeIfPaused();
        }
    }

    public void removeBehaviour(Behaviour... behaviours) {
        removeBehaviour(Arrays.asList(behaviours));
    }
}
