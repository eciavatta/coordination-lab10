package it.unibo.fol.agents.behaviours;

import it.unibo.fol.agents.AgentController;

import java.util.Collection;
import java.util.Deque;
import java.util.LinkedList;
import java.util.stream.Collectors;

class DoWhile extends Sequence {

    private final Deque<Behaviour> behavioursBackup;
    private boolean isEndOfRound = false;

    public DoWhile(Behaviour b, Behaviour... bs) {
        super(b, bs);
        behavioursBackup = getSubBehaviours().stream().map(Behaviour::deepClone).collect(Collectors.toCollection(LinkedList::new));
    }

    public DoWhile(Collection<Behaviour> bs) {
        super(bs);
        behavioursBackup = getSubBehaviours().stream().map(Behaviour::deepClone).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public Behaviour deepClone() {
        return new DoWhile(getSubBehaviours().stream().map(Behaviour::deepClone).collect(Collectors.toList()));
    }

    @Override
    public void execute(AgentController ctl) throws Exception {
        isEndOfRound = false;
        super.execute(ctl);
        if (countResidualSubBehaviours() == 0) {
            isEndOfRound = true;
            addSubBehaviours(behavioursBackup.stream().map(Behaviour::deepClone).collect(Collectors.toList()));
        }
    }

    @Override
    public boolean isOver() {
        return isEndOfRound && !condition();
    }

    public boolean condition() {
        return true;
    }

}
