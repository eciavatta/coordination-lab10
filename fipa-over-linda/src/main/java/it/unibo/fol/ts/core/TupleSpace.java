package it.unibo.fol.ts.core;

import java.util.Collection;
import java.util.concurrent.CompletableFuture;

public interface TupleSpace<T extends Tuple, TT extends Template> {
    CompletableFuture<T> read(TT template);

    CompletableFuture<T> take(TT template);

    CompletableFuture<T> write(T tuple);

    CompletableFuture<Collection<? extends T>> get();

    CompletableFuture<Integer> getSize();

    String getName();
}