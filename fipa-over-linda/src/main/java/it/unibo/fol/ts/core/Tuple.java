package it.unibo.fol.ts.core;

public interface Tuple {
    default boolean matches(final Template template) {
        return template.matches(this);
    }
}
