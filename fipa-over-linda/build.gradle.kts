plugins {
    `java-library`
}

group = rootProject.group
version = rootProject.version

val javaVersion: String by project
val tuprologVersion: String by project
val namedRegexpVersion: String by project
val junitVersion: String by project

dependencies {
    api(project(":fipa-over-linda"))
    api(project(":test-utils"))
    api(project(":tusow"))

    api("io.vertx", "vertx-core", "3.5.4")
    api("io.vertx", "vertx-web", "3.5.4")
    api("it.unibo.alice.tuprolog", "tuprolog", tuprologVersion)

    implementation("it.unibo.tusow", "ws", "0.2.+")
    implementation("org.javatuples", "javatuples", "1.2")

    testImplementation("junit", "junit", junitVersion)
    // testImplementation(project(":linda-test"))
}

configure<JavaPluginConvention> {
    targetCompatibility = JavaVersion.valueOf("VERSION_$javaVersion")
    sourceCompatibility = JavaVersion.valueOf("VERSION_$javaVersion")
}